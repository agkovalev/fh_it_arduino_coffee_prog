
#include "string.h"
#include "EEPROM.h"
// #include <MemoryFree.h>

/**
 *
 */
#include "/home/agkovalev/sketchbook/fh_it_coffee_prog/parts/globals.cpp"



/**
 * [setup description]
 * @return {[type]} [description]
 */
void setup(){

	Serial.begin(9600);

	// Setup pins
	// pinMode(LED_PIN, OUTPUT);

}


/**
 * Include parts
 */
#include "/home/agkovalev/sketchbook/fh_it_coffee_prog/parts/receipts.cpp"
#include "/home/agkovalev/sketchbook/fh_it_coffee_prog/parts/commandline.cpp"


/**
 * [loop description]
 * @return {[type]} [description]
 */
void loop(){

	commandLine::proccess();

	// Serial.print("freeMemory()=");
	// Serial.println(freeMemory());

	delay(1000);
}
