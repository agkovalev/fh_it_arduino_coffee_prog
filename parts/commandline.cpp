namespace commandLine {

	/**
	 *
	 */
	#define COMMAND_ADD					"add"
	#define COMMAND_EDIT				"edit"
	#define COMMAND_DELETE				"delete"
	#define COMMAND_GET					"get"
	#define COMMAND_CLEAN				"clean"
	#define COMMAND_SHOW_MEMORY			"show"


	/**
	 *
	 */
	char input[36];

	/**
	 *
	 */
	char parsedCommand[17][7];



	/**
	 * [fillBuffer description]
	 */
	void fillBuffer(){
		byte i = 0;

		while (Serial.available()) {
			// get the new byte:
			char inChar = (char) Serial.read();

			// add it to the inputString:
			input[i] = inChar;
			i++;
		}
	}

	/**
	 * [clearBuffer description]
	 */
	void clearBuffer(){
		for (int i = 0; i < 36; ++i){
			input[i] = NULL;
		}
	}


	/**
	 * [checkCommand description]
	 * @param  str [description]
	 * @return     [description]
	 */
	bool checkCommand(char * str, char * entered){
		if(entered[0] == NULL)
			return false;
		if(strncmp(str, entered, strlen(str)) == 0)
			return true;

		return false;
	}


	/**
	 * Функция добавления нового рецепта напитка
	 * Переданные аргументы заполняют соответствующие ячейки памяти
	 * TODO: необходимо определеить стартовую ячейку для нового напитка.
	 * Если не осталось места, выдать сообщение
	 * Если не осталось кнопок на пульте - сообщить
	 * @return bool		true, если удалось добавить
	 */
	bool addReceipt(){
		int aData[16];
		byte limitForPriceIntegers = 99;

		for(int i = 0; i < 17; i++){
			if(parsedCommand[i+1][0] != NULL){
				byte cellValue = atoi(parsedCommand[i+1]);
				if(i == receipts::CELL_PRICE_EURO || i == receipts::CELL_PRICE_CENT){
					if(cellValue > limitForPriceIntegers)
						cellValue = limitForPriceIntegers;
				}
				aData[i] = cellValue;
			}
			else
				aData[i] = 0;
			// Serial.println(aData[i]);
		}

		if(receipts::add(aData)){
			Serial.println("receipt added!");
			return true;
		}

		return false;
	}


	/**
	 * Вывод на экран информации о сохраненном ранее рецепте
	 * @param  {[type]} String *aArguments   [description]
	 * @return {[type]}        [description]
	 */
	void getReceipt(){
		byte btnNum, cellRelativeNum = 255;
		btnNum = atoi(parsedCommand[1]);
		if(parsedCommand[2] != 0)
			cellRelativeNum = atoi(parsedCommand[2]);
		Serial.println(cellRelativeNum);
		if(btnNum && (255 == cellRelativeNum || 0 == cellRelativeNum)){
			receipts::get(btnNum);
		}
		else{
			receipts::getCellValue(btnNum, cellRelativeNum);
		}
	}


	/**
	 * [EditReceipt description]
	 * @param  {[type]} String *aArguments   [description]
	 * @return {[type]}        [description]
	 */
	bool editReceipt(){
		byte btnNum, cellNum, cellNewVal, limitForPriceIntegers = 99;

		btnNum		= atoi(parsedCommand[1]);
		cellNum		= atoi(parsedCommand[2]);
		cellNewVal	= atoi(parsedCommand[3]);

		// Если пытаемся поменять не свою ячейку
		if(cellNum >= receipts::CELLS_PRO_OBJECT){
			Serial.println("Error! Wrong property number given!");
			return false;
		}

		// Если назначаем не правильную кнопку
		if(cellNum == receipts::CELL_BTN_NUM && (cellNewVal <= 0 || cellNewVal > 9)){
			Serial.println("Error! Wrong button number!");
			return false;
		}

		// Если зачем-то пытаемся перезаписать одно и то же значение в ячейку кнопки
		if(cellNum == receipts::CELL_BTN_NUM && cellNewVal == btnNum){
			Serial.println("Error! Why are you trying to do this?!");
			return false;
		}

		// Если пытаемся назначить занятую кнопку
		if(cellNum == receipts::CELL_BTN_NUM && (receipts::MAX_MEMORY_CELLS != receipts::search(0, cellNewVal))){
			Serial.println("Error! This button number is not free!");
			return false;
		}

		// Если пытаемся назначить занятую кнопку
		if(cellNum == receipts::CELL_PRICE_EURO || cellNum == receipts::CELL_PRICE_CENT){
			if(cellNewVal > limitForPriceIntegers){
				cellNewVal = limitForPriceIntegers;
				Serial.println("Warning! The number you have entered is out of range 0-99! Value was saved as 99");
			}
			return false;
		}

		// Если удалось отредактировать
		if(receipts::edit(btnNum, cellNum, cellNewVal)){
			Serial.println("receipt edited!");
			return true;
		}

		// В любом другом случае
		return false;
	}


	/**
	 * [deleteReceipt description]
	 * @return [description]
	 */
	bool deleteReceipt(){
		byte btnNum;

		btnNum = atoi(parsedCommand[1]);

		// Если даем не правильную кнопку
		if(btnNum <= 0 || btnNum > 9){
			Serial.println("Error! Button number is out of allowed range!");
			return false;
		}

		// Если такой номер кнопки не был назначен ни одному сохраненному напитку
		if(receipts::MAX_MEMORY_CELLS == receipts::search(0, btnNum)){
			Serial.println("Error! This button number was not found!");
			return false;
		}

		// Если удалось удалить
		if(receipts::del(btnNum)){
			Serial.println("receipt deleted!");
			return true;
		}

		// В любом другом случае
		return false;

	}


	/**
	 * [CleanMemory description]
	 * @return {[type]} [description]
	 */
	void cleanMemory(){
		if(receipts::clean())
			Serial.println("Memory was reseted to 0");
		else
			Serial.println("Operation aborted");
	}


	/**
	 * [ShowMemory description]
	 * @return {[type]} [description]
	 */
	void showMemory(){
		unsigned short from = 0, to = 1024;
		if(parsedCommand[1] != NULL)
			from = atoi(parsedCommand[1]);
		if(parsedCommand[2] != NULL)
			to = atoi(parsedCommand[2]);
		for(byte i = from; i < to; i++){
			Serial.print(i);
			Serial.print(": ");
			Serial.print(EEPROM.read(i));
			Serial.print(",\t");
		}
	}


	/**
	 * Парсим введенную строку и возвращаем структуру из имени команды и массива ее аргументов
	 * @param 	String inputString 		введенная строка
	 * @return 	struct structCommand 	структура из имени команды и массива ее аргументов
	 */
	void getCommandArray(){

		// Индекс следующего найденного пробела (разделителя)
		char * pch;
		// Счетчик найденных слов
		byte wordsCounter = 0;

		// 1. На всякий случай сначала чистим массив аргументов (пишем нули)
		for(byte i = 0; i < 16; i++)
			for(byte j = 0; j < 8; j++)
				parsedCommand[i][j] = NULL;

		// 2. Запускаем цикл поиска слов (количество слов не больше 17)

		pch = strtok (input," ,.-");
		while (pch != NULL && wordsCounter < 17){
			strcpy(parsedCommand[wordsCounter], pch);
			pch = strtok (NULL, " ,.-");
			wordsCounter++;
		}

	}


	/**
	 * [proccess description]
	 */
	void proccess(){

		// 1. Мигает светодиод при низкой частоте
		// 2. Считываем команды:
		// 		2.1. exit - выход из режима в Нормальный режим, обнуление счетчиков и наполнение ингридиентов
		// 		2.2. add [значения для ячеек памяти в соответствии с определенной структурой, разделены пробелом] - добавление напитка
		// 		2.3. edit [номер кнопки на пульте] [номер ячейки памяти(относительный)] [значение] редактирование (если напиток не найден, выдаем ошибку)
		// 		2.4. delete [номер кнопки на пульте] - удаление напитка
		// 		2.5. get [номер кнопки на пульте] - получение полной информации о сохраненном напитке
		// 		2.6. clean - очищает память. Требует дополнительное подтверждение (нужно ввести yes)
		// 		2.7. show [from cell] [to cell]- показывает ячейки памяти.


		fillBuffer();

		// Если строка не пустая
		while(input[0] != NULL){

			getCommandArray();

			if(checkCommand(COMMAND_ADD, parsedCommand[0])){
				addReceipt();
			}
			else if(checkCommand(COMMAND_EDIT, parsedCommand[0])){
				editReceipt();
			}
			else if(checkCommand(COMMAND_DELETE, parsedCommand[0])){
				deleteReceipt();
			}
			else if(checkCommand(COMMAND_GET, parsedCommand[0])){
				getReceipt();
			}
			else if(checkCommand(COMMAND_CLEAN, parsedCommand[0])){
				cleanMemory();
			}
			else if(checkCommand(COMMAND_SHOW_MEMORY, parsedCommand[0])){
				showMemory();
			}
			else{
				Serial.print(parsedCommand[0]);
				Serial.println(" is unknown command!");
				Serial.println("You can enter one of these commands:");
				Serial.print(COMMAND_ADD);
				Serial.println(" [values of receipt data] - add new receipt");
				Serial.print(COMMAND_EDIT);
				Serial.println(" [drink number on the controller] [relative number of the memory cell] [value] - edit receipt by drink control number");
				Serial.print(COMMAND_DELETE);
				Serial.println(" [drink number on the controller] - delete receipt by drink control number");
				Serial.print(COMMAND_GET);
				Serial.println(" [drink number on the controller] [relative cell number] - get info about receipt by drink control number");
				Serial.print(COMMAND_CLEAN);
				Serial.println(" - clean memory");
				Serial.print(COMMAND_SHOW_MEMORY);
				Serial.println("[start byte] [end byte] - show memory (for example start 0 1024, or start 16 32). If no arguments passed, all of 1024 bytes will be displayed.");
			}

			// Очищаем строку
			clearBuffer();
		}

	}

}
