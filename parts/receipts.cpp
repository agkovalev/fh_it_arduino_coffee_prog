/**
 * Функции и переменные по работе с рецептами напитков в памяти
 */
namespace receipts{

	/**
	 *
	 */
	const byte CELLS_PRO_OBJECT				= 16;
	const byte MAX_COUNT_OBJECTS			= 9;
	const unsigned short MAX_MEMORY_CELLS	= 1024;

	/**
	 * Перечисление назначения ячеек памяти в каждом блоке, начиная с 0
	 * Пригодится при работе с памятью в нормальном режиме автомата
	 */
	enum Cells {
		CELL_BTN_NUM,
		CELL_INGRIDIENTS_COFFEE,
		CELL_INGRIDIENTS_MILK,
		CELL_PRICE_EURO,
		CELL_PRICE_CENT,
		CELL_COUNTER
	} memoryCells;

	/**
	 * Cache for selected receipt data
	 */
	byte currentReceiptData[16];



	/**
	 * Найти номер начальной ячейки памяти для рецепта по критерию (определенное значение)
	 * @param  {[type]} byte cellNum       [description]
	 * @param  {[type]} byte cellVal       [description]
	 * @return {[type]}      [description]
	 */
	unsigned short search(byte cellNum, byte cellVal){

		for(byte i = 0, addr = 0; i < MAX_COUNT_OBJECTS; i++){
			// Реальный номер проверяемой ячейки памяти для конкретного рецепта
			addr = i * CELLS_PRO_OBJECT + cellNum;

			// Если нашли совпадение, сразу выводим номер начальной ячейки
			if(EEPROM.read(addr) == cellVal)
				return i * CELLS_PRO_OBJECT;
		}
		return MAX_MEMORY_CELLS;

	}


	/**
	 * [resetTempReceipt description]
	 * @return {[type]} [description]
	 */
	void resetTempReceipt(){

		for(byte i = 0; i < CELLS_PRO_OBJECT; i++){
		    currentReceiptData[i] = 0;
		}

	}


	/**
	 * Получить информацию из памяти о рецепте
	 * @param  {[type]} byte btnnum	номер кнопки на пульте
	 * @return {[type]}				массив значений
	 */
	void get(byte btnNum){

		// Если номер кнопки не подходит, выдаем ошибку
		if(btnNum > MAX_COUNT_OBJECTS || btnNum == 0){
			Serial.println("The number of button you have entered is out of range 1-9");
			resetTempReceipt();
		}
		else {
			unsigned short startCell = 0;

			startCell = search(CELL_BTN_NUM, btnNum);
			// Если не нашли, сообщаем
			if(startCell >= MAX_MEMORY_CELLS){
				Serial.println("Receipt not found!");
				resetTempReceipt();
			}
			else{
				// Если нашли, возвращаем инфу
				for(int i = 0, addr = startCell; i < CELLS_PRO_OBJECT; i++, addr++){
					if(addr < MAX_MEMORY_CELLS){
						// Пишем в кэш
						currentReceiptData[i] = EEPROM.read(addr);
						// Пишем на экран
						Serial.print(i);
						Serial.print("(");
						Serial.print(addr);
						Serial.print(") = ");
						Serial.print(currentReceiptData[i]);
						Serial.println("");
					}
				}
			}
		}

	}


	/**
	 * Получить информацию из памяти о рецепте
	 * @param  {[type]} byte btnnum	номер кнопки на пульте
	 * @return {[type]}				массив значений
	 */
	void getCellValue(byte btnNum, byte cellNum){

		// Если номер кнопки не подходит, выдаем ошибку
		if(btnNum > MAX_COUNT_OBJECTS || btnNum == 0){
			Serial.println("The number of button you have entered is out of range 1-9");
			// resetTempReceipt();
		}
		else {
			unsigned short startCell = 0;

			startCell = search(CELL_BTN_NUM, btnNum);
			// Если не нашли, сообщаем
			if(startCell >= MAX_MEMORY_CELLS){
				Serial.println("Receipt not found!");
				// resetTempReceipt();
			}
			else{
				// Пишем на экран
				Serial.print(cellNum);
				Serial.print("(");
				Serial.print(startCell + cellNum);
				Serial.print(") = ");
				Serial.print(EEPROM.read(startCell + cellNum));
				Serial.println("");
			}
		}

	}


	/**
	 * [add description]
	 * @param {[type]} String * aData [description]
	 */
	bool add(int * aData){

		// 1. Проверить наличие напитка с той же кнопкой (если не найден, будет равно 1024)
		if(MAX_MEMORY_CELLS == search(CELL_BTN_NUM, aData[0])){
			unsigned short startCell = 0;

			// Найти ближайшую ячейку с нулевым значением номера кнопки пульта
			startCell = search(CELL_BTN_NUM, 0);
			// она не должна быть равна объему памяти
			if(MAX_MEMORY_CELLS != startCell){
				for(int i = 0, cell = startCell; i < CELLS_PRO_OBJECT; i++, cell++){
					EEPROM.write(cell, aData[i]);
					Serial.println(aData[i]);
				}
				return true;
			}
			else{
				Serial.println("Create new receipt failed! There is no place in memory!");
			}
		}
		else{
			Serial.println("Create new receipt failed! There is a saved receipt with a same button number!");
		}
		return true;

	}


	/**
	 * [edit description]
	 * @param  {[type]} byte btnNum        [description]
	 * @param  {[type]} byte cellNum       [description]
	 * @param  {[type]} byte cellVal       [description]
	 * @return {[type]}      [description]
	 */
	bool edit(byte btnNum, byte cellNum, byte cellVal){
		unsigned short startCell = 0;

		// Проверка правильности ввода номера ячейки была проведена в вызывающей функции

		// Проверить наличие напитка с этой кнопкой (если найден, будет равно 1024)
		startCell = search(CELL_BTN_NUM, btnNum);
		if(MAX_MEMORY_CELLS != startCell){
			EEPROM.write((startCell + cellNum), cellVal);
			return true;
		}
		else{
			Serial.println("Edit saved receipt failed! There is no receipt with given button number found!");
		}

		return false;

	}


	/**
	 * [del description]
	 * @param  {[type]} byte btnNum        [description]
	 * @return {[type]}      [description]
	 */
	bool del(byte btnNum){
		unsigned short startCell = 0;

		startCell = search(CELL_BTN_NUM, btnNum);
		if(MAX_MEMORY_CELLS != startCell){
			for(int i = 0, cell = startCell; i < CELLS_PRO_OBJECT; i++, cell++){
				EEPROM.write(cell, 0);
			}
			return true;
		}
		else{
			Serial.println("Delete saved receipt failed! There is no receipt with given button number found!");
		}
		return false;

	}


	/**
	 * [clean description]
	 * @return {[type]} [description]
	 */
	bool clean(){

		for(int i = 0; i < MAX_MEMORY_CELLS; i++){
			EEPROM.write(i, 0);
			Serial.print(i);
			Serial.print(": ");
			Serial.print(EEPROM.read(i));
			Serial.print(", ");
		}
		return true;
	}

}
